package com.stackroute.datamunger.reader;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.LineNumberReader;
import java.util.regex.Pattern;

import com.stackroute.datamunger.query.DataTypeDefinitions;
import com.stackroute.datamunger.query.Header;

public class CsvQueryProcessor extends QueryProcessingEngine {

	protected String fileName;
	BufferedReader bufferedReader;	

	/*
	 * parameterized constructor to initialize filename. As you are trying to
	 * perform file reading, hence you need to be ready to handle the IO Exceptions.
	 */
	public CsvQueryProcessor(String fileName) throws FileNotFoundException {
		this.fileName = fileName;
		bufferedReader = new BufferedReader(new FileReader(fileName));
	}

	/*
	 * implementation of getHeader() method. We will have to extract the headers
	 * from the first line of the file.
	 */
	@Override
	public Header getHeader() throws IOException {
		bufferedReader = new BufferedReader(new FileReader(fileName));
		// read the first line				
		String []headercoloumnData = bufferedReader.readLine().split(",");
		
		// populate the header object with the String array containing the header names
		Header h = new Header();
		h.setHeaders(headercoloumnData);	
		h.getHeaders();		
		return h;
	}
	

	/**
	 * This method will be used in the upcoming assignments
	 */
	@Override
	public void getDataRow() {
		String line;
		try {
			bufferedReader = new BufferedReader(new FileReader(this.fileName));
		} catch (FileNotFoundException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		LineNumberReader count = null;
		try {
			count = new LineNumberReader(new FileReader(this.fileName));
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String dataRow[] = new String[count.getLineNumber()];
		int i = 0;
		try {
			while((line = bufferedReader.readLine())!= null)
			{
				dataRow[i] = line;
				System.out.println(line);
				i++;
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * implementation of getColumnType() method. To find out the data types, we will
	 * read the first line from the file and extract the field coloumnData from it. In
	 * the previous assignment, we have tried to convert a specific field value to
	 * Integer or Double. However, in this assignment, we are going to use Regular
	 * Expression to find the appropriate data type of a field. Integers: should
	 * contain only digits without decimal point Double: should contain digits as
	 * well as decimal point Date: Dates can be written in many formats in the CSV
	 * file. However, in this assignment,we will test for the following date
	 * formats('dd/mm/yyyy',
	 * 'mm/dd/yyyy','dd-mon-yy','dd-mon-yyyy','dd-month-yy','dd-month-yyyy','yyyy-mm-dd')
	 */
	@Override
	public DataTypeDefinitions getColumnType() throws IOException {
		
		bufferedReader = new BufferedReader(new FileReader(fileName));		
	
		// checking for Integer
		Pattern intCond = Pattern.compile("[0-9]+");
		
		// checking for floating point numbers
		Pattern doubleCond = Pattern.compile("[0-9][\\.][0-9]");
		
		// checking for date format dd/mm/yyyy
		// checking for date format mm/dd/yyyy
		Pattern dateCond = Pattern.compile("[0-9]{2}/[0-9]{2}/[0-9]{4}");		
				
		// checking for date format dd-mon-yy
		Pattern dateCond1 = Pattern.compile("[0-9]{2}-[a-zA-Z]{3}-[0-9]{2}");
				
		// checking for date format dd-mon-yyyy
		Pattern dateCond2 = Pattern.compile("[0-9]{2}-[a-zA-Z]{3}-[0-9]{4}");
		
		// checking for date format dd-month-yy
		Pattern dateCond3 = Pattern.compile("[0-9]{2}-[a-zA-Z]-[0-9]{2}");
		
		// checking for date format dd-month-yyyy
		Pattern dateCond4 = Pattern.compile("[0-9]{2}-[a-zA-Z]-[0-9]{4}");
		
		// checking for date format yyyy-mm-dd
		Pattern dateCond5 = Pattern.compile("[0-9]{4}-[0-9]{2}-[0-9]{2}");		
		
		
		String line = bufferedReader.readLine();
		String dataRow = bufferedReader.readLine();
		
		if(dataRow.endsWith(","))
		{
			dataRow = dataRow + " ";
		}
		String coloumnData[] = dataRow.split(",");
		String [] dataType = new String[coloumnData.length];
		int index = 0;
		while(index < coloumnData.length)
		{
			
			
			if(intCond.matcher(coloumnData[index]).matches())
			{
				dataType[index] = "java.lang.Integer";
			}
			
			else if(doubleCond.matcher(coloumnData[index]).matches())
			{
				dataType[index] = "java.lang.Double";
			}
			else if(dateCond.matcher(coloumnData[index]).matches() || dateCond1.matcher(coloumnData[index]).matches() || dateCond2.matcher(coloumnData[index]).matches() || dateCond3.matcher(coloumnData[index]).matches() || dateCond4.matcher(coloumnData[index]).matches() || dateCond5.matcher(coloumnData[index]).matches())
			{
				dataType[index] = "java.util.Date";
			}
			else if(coloumnData[index].equalsIgnoreCase(" "))
			{
				dataType[index] = "java.lang.Object";
			}
			else
			{					
				dataType[index] = "java.lang.String";			
			}
			index++;
		}
		
		DataTypeDefinitions dataTypeDefinitions = new DataTypeDefinitions();
		dataTypeDefinitions.setDataTypes(dataType);
		dataTypeDefinitions.getDataTypes();
		return dataTypeDefinitions;
	}
	
	

	
	

}
